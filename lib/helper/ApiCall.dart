import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;

Future<http.Response> fetchDatas(String route) async {
  http.Response response = await http
      .get(
    route,
    headers: await getHeaderToken(),
  )
      .catchError((error) {
    throw new Exception("$error");
  });

  //expired token
  if (response.statusCode == 401) {
    await refreshToken();
    return http.get(
      route,
      headers: await getHeaderToken(),
    );
  }

  return response;
}

Future<http.Response> postDatas(String route, String data) async {
  http.Response response = await http
      .post(
    route,
    body: data,
    headers: await getHeaderToken(),
  )
      .catchError((error) {
    throw new Exception("$error");
  });

  //expired token
  if (response.statusCode == 401) {
    await refreshToken();
    return http.post(
      route,
      body: data,
      headers: await getHeaderToken(),
    );
  }

  return response;
}

Future<http.Response> putDatas(String route, String data) async {
  http.Response response = await http
      .put(
    route,
    body: data,
    headers: await getHeaderToken(),
  )
      .catchError((error) {
    throw new Exception("$error");
  });

  //expired token
  if (response.statusCode == 401) {
    await refreshToken();
    return http.post(
      route,
      body: data,
      headers: await getHeaderToken(),
    );
  }

  return response;
}

Future<Map<String, String>> getHeaderToken() async {
  String token = '';

  final storage = new FlutterSecureStorage();
  await storage.read(key: 'apiToken').then(
    (value) {
      if (value != null) {
        token = value;
      }
    },
  );
  //print('Header token $token');
  return {
    //the await is necessary to until we have the String token before
    HttpHeaders.authorizationHeader: "Bearer ${token.trim()}",
    HttpHeaders.acceptHeader: "application/ld+json",
    HttpHeaders.contentTypeHeader: "application/json"
  };
}

Future refreshToken() async {
  final storage = new FlutterSecureStorage();
  await storage.read(key: 'apiRefreshToken').then(
    (value) async {
      if (value != null) {
        http.Response response = await http.post(
          'http://3.125.208.58:3081/refresh_token',
          body: '{"refresh_token": "$value"}',
          headers: {HttpHeaders.contentTypeHeader: "application/json"},
        ).catchError((error) {
          throw new Exception("$error");
        });

        if (response.statusCode == 200) {
          final tokenResponse = jsonDecode(response.body);
          final storage = new FlutterSecureStorage();
          storage.write(key: 'apiToken', value: tokenResponse['token']);
          storage.write(
              key: 'apiRefreshToken', value: tokenResponse['refresh_token']);
        }
      }
    },
  );
}

Future<Map<String, dynamic>> generateToken(
    String username, String password) async {
  http.Response response = await http.post(
    'http://3.125.208.58:3081/login_check',
    body: '{"email": "$username", "password": "$password"}',
    headers: {HttpHeaders.contentTypeHeader: "application/json"},
  );

  Map<String, dynamic> callResponse = {"code": 200, "message": ""};

  if (response.statusCode == 200) {
    final tokenResponse = jsonDecode(response.body);
    final storage = new FlutterSecureStorage();
    await storage.write(key: 'apiToken', value: tokenResponse['token']);
    await storage.write(
        key: 'apiRefreshToken', value: tokenResponse['refresh_token']);
  } else if (response.statusCode == 401) {
    callResponse["code"] = 401;
    callResponse["message"] = "Adresse mail ou mot de passe incorrect.";
  } else {
    print('Some error occured =====================');
    print('Error code : ${response.statusCode}');
    print('Error text ${response.body}');
    print('=====================');

    callResponse["code"] = response.statusCode;
    callResponse["message"] =
        "Impossible de se connecter. Veuillez contacter un administrateur.";
  }

  return callResponse;
}
