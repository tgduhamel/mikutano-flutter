import 'dart:ffi';

import 'package:intl/intl.dart';

String stringToHumanDate(int year, int month, int day) {
  final df = new DateFormat('dd/MM/yyyy');
  return df.format(new DateTime.utc(year, month, day));
}

String stringToServerDate(int year, int month, int day) {
  final df = new DateFormat('yyyy-MM-dd');
  return df.format(new DateTime.utc(year, month, day));
}

String stringToHumanTime(int hour, int minutes) {
  String h = (hour < 10) ? "0${hour.toString()}" : hour.toString();
  String m = (minutes < 10) ? "0${minutes.toString()}" : minutes.toString();

  return "$h:$m";
}

String fromDayToYear(String dateFromDatePicker) {
  var date = dateFromDatePicker.split("/");
  return stringToServerDate(int.parse(date[2]), int.parse(date[1]), int.parse(date[0]));
}

String fromYearToDay(String dateFromApi) {
  var date = dateFromApi.split("-");
  return stringToHumanDate(int.parse(date[0]), int.parse(date[1]), int.parse(date[2]));
}