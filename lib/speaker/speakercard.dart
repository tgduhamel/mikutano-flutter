import 'package:flutter/material.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/models/Speaker.dart';
import 'package:mikutano/speaker/speakerform.dart';

class SpeakerCard extends StatelessWidget {
  final int id;
  final String name;
  final String email;
  final String phone;

  const SpeakerCard(
      {Key key,
      @required this.id,
      @required this.name,
      @required this.email,
      @required this.phone})
      : super(key: key);

  factory SpeakerCard.fromJson(Participant speaker) {
    return SpeakerCard(
        id: speaker.id,
        name: speaker.name,
        email: speaker.mail,
        phone: speaker.phone);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Icon(
          Icons.account_circle,
          size: 60.0,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                this.name,
                style: TextStyle(
                    fontSize: 25.0, fontWeight: FontWeight.normal),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, bottom: 5.0),
              child: Text(
                this.email,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, bottom: 5.0),
              child: Text(
                this.phone,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
