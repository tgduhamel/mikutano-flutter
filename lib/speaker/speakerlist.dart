import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/speaker/speakerform.dart';
import 'package:pk_skeleton/pk_skeleton.dart';
import 'speakercard.dart';

class SpeakerList extends StatefulWidget {
  SpeakerList({Key key}) : super(key: key);

  @override
  _SpeakerListState createState() => _SpeakerListState();
}

class _SpeakerListState extends State<SpeakerList> {
  static GlobalKey<ScaffoldState> _speakerListScaffoldKey =
      GlobalKey<ScaffoldState>();
  List<Participant> _speakersList = [];

  bool _loading = true;
  bool _hasDataChanged = false;

  @override
  void initState() {
    getSpeakersList();
    super.initState();
  }

  getSpeakersList() {
    return fetchDatas('http://3.125.208.58:3081/api/speakers').then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 200) {
        setState(() {
          _loading = false;
        });
        final parsed = (jsonDecode(value.body)['hydra:member'])
            .cast<Map<String, dynamic>>();
        _speakersList = parsed
            .map<Participant>((json) => Participant.fromJson(json))
            .toList();
      } else {
        print(value.body);
        _speakerListScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données.',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _speakerListScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Liste intervenants (${_speakersList.length})'),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: _loading
            ? PKCardSkeleton(
                isBottomLinesActive: true,
              )
            : _speakersList.length == 0
                ? Center(
                    child: Text(
                      'Aucune donnée disponible.',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                : RefreshIndicator(
                    child: ListView(
                      physics: AlwaysScrollableScrollPhysics(),
                      children: _speakersList
                          .map((Participant speaker) {
                            return Card(
                              child: FlatButton(
                                  child: new SpeakerCard.fromJson(speaker),
                                  onPressed: () async {
                                    _hasDataChanged = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => SpeakerForm(
                                          idSpeaker: speaker.id,
                                        ),
                                      ),
                                    );

                                    if (_hasDataChanged != null && _hasDataChanged) {
                                      getSpeakersList();
                                    }
                                  }),
                            );
                          })
                          .toList()
                          .cast<Widget>(),
                    ),
                    onRefresh: () {
                      return getSpeakersList();
                    },
                  ),
      ),
      floatingActionButton: _loading
          ? null
          : FloatingActionButton(
              onPressed: () async {
                _hasDataChanged = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SpeakerForm(),
                  ),
                );

                if (_hasDataChanged != null && _hasDataChanged) {
                  getSpeakersList();
                }
              },
              child: Icon(Icons.add),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
    );
  }
}
