import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:mikutano/components/InputFields.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/homepage.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  static GlobalKey<ScaffoldState> _signInScaffoldKey =
      GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  bool _requesting = false;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();

  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _signInScaffoldKey,
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.only(
            top: 20.0,
            bottom: 20.0,
          ),
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color>[
                //Color.fromRGBO(162, 146, 199, 0.8),
                //Color.fromRGBO(51, 51, 63, 0.9),
                Color.fromRGBO(162, 146, 199, 0.2),
                Color.fromRGBO(51, 51, 63, 0.8),
              ],
              stops: [0.2, 1.0],
            ),
          ),
          child: ListView(
            padding: EdgeInsets.all(0.0),
            children: <Widget>[
              Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        width: 250.0,
                        height: 250.0,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: ExactAssetImage('assets/images/profile.png'),
                            fit: BoxFit.scaleDown,
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Form(
                              key: _formKey,
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  InputFieldArea(
                                    hint: "Adresse mail",
                                    obscure: false,
                                    icon: Icons.person_outline,
                                    txtController: emailController,
                                    txtInputType: TextInputType.emailAddress,
                                  ),
                                  InputFieldArea(
                                    hint: "Mot de passe",
                                    obscure: true,
                                    icon: Icons.lock_outline,
                                    txtController: passwordController,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      FlatButton(
                        padding: EdgeInsets.only(
                          top: 160.0,
                          bottom: 30.0,
                        ),
                        onPressed: null,
                        child: Text(
                          'Mot de passe oublié',
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          softWrap: true,
                          style: TextStyle(
                            fontWeight: FontWeight.w300,
                            letterSpacing: 0.5,
                            color: Colors.white,
                            fontSize: 12.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.only(bottom: 70.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                          child: _requesting
                              ? CircularProgressIndicator(
                                  backgroundColor: Color(0xFF98ee99),
                                  value: null,
                                )
                              : null,
                        ),
                        FlatButton(
                          disabledColor: Color(0xFF98ee99),
                          color: Color(0xFF66bb6a),
                          shape: StadiumBorder(),
                          padding: EdgeInsets.only(
                            top: 20.0,
                            bottom: 20.0,
                            left: 60.0,
                            right: 60.0,
                          ),
                          onPressed: _requesting
                              ? null
                              : () {
                                  if (_formKey.currentState.validate()) {
                                    setState(() {
                                      _requesting = true;
                                    });
                                    var apiResponse = generateToken(emailController.text.trim(),passwordController.text.trim());
                                    apiResponse.then((response) {
                                      if (response["code"] == 200) {
                                        setState(() {
                                          _requesting = false;
                                        });
                                        Navigator.popUntil(context, ModalRoute.withName('/'));
                                        Navigator.pushReplacementNamed(context, '/');
                                      } else {
                                        _signInScaffoldKey.currentState
                                            .showSnackBar(
                                          SnackBar(
                                            content: Text(
                                              '${response['message']}',
                                            ),
                                            backgroundColor: Color(0xFFB00020),
                                          ),
                                        );
                                        setState(() {
                                          _requesting = false;
                                        });
                                      }
                                    });
                                  }
                                },
                          child: Text(
                            "Connexion",
                            style: TextStyle(
                              color: Color(0xFF000000),
                              fontSize: 20.0,
                              fontWeight: FontWeight.w300,
                              letterSpacing: 0.3,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
