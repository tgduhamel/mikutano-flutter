import 'package:flutter/material.dart';

class MenuCard extends StatelessWidget {
  final IconData icon;
  final double sizeIcon;
  final String title;
  final Color cardColor;
  final String route;

  const MenuCard({
    Key key,
    @required this.icon,
    @required this.sizeIcon,
    @required this.title,
    @required this.cardColor,
    @required this.route,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        height: 150.0,
        constraints: BoxConstraints(minWidth: 180.0),
        child: Card(
          color: this.cardColor,
          child: FlatButton(
            onPressed: () {
              Navigator.pushNamed(context, this.route);
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Icon(
                  this.icon,
                  size: this.sizeIcon,
                  color: Colors.white,
                ),
                Text(
                  this.title,
                  style: TextStyle(
                    color: Colors.white,
                    letterSpacing: 2.0,
                    fontSize: 18.0,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
