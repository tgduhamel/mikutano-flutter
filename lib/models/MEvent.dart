import 'package:intl/intl.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/models/Participant.dart';

class MEvent {
  int key;
  String title;
  String address;
  String startDate;
  String startHour;
  String endDate;
  String endHour;
  List speakers;
  List participants;

  MEvent(key, title, address, startDate, startHour, endDate, endHour, speakers,
      participants) {
    this.key = key;
    this.title = title;
    this.address = address;
    this.startDate = startDate;
    this.startHour = startHour;
    this.endDate = endDate;
    this.endHour = endHour;
    this.speakers = speakers;
    this.participants = participants;
  }

  factory MEvent.fromJson(Map<String, dynamic> json) {
    return MEvent(
      json['id'],
      json['title'],
      json['address'],
      json['dateStart'],
      json['hourStart'],
      json['dateEnd'],
      json['hourEnd'],
      json['participants'],
      json['speakers'],
    );
  }
}
