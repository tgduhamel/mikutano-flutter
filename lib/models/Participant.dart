class Participant {
  int id;
  String uri;
  String name;
  String mail;
  String phone;

  Participant(key, uri, name, mail, phone) {
    this.id = key;
    this.uri = uri;
    this.name = name;
    this.mail = mail;
    this.phone = phone;
  }

  factory Participant.fromJson(Map<String, dynamic> json) {
    return Participant(
      json['id'],
      json['@id'],
      json['name'],
      json['email'],
      json['phone']
    );
  }

  @override
  // ignore: hash_and_equals
  bool operator == (other) {
    return (other.id == id && other.name == name && other.mail == mail && other.phone == phone);
  }
}