import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'menucard.dart';

class HomePage extends StatelessWidget {
  HomePage({Key key}) : super(key: key);

  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                child: AutoSizeText(
                  'Mikutano',
                  style: TextStyle(
                    fontFamily: 'Arizonia',
                    fontSize: 90.0,
                    letterSpacing: 5.0,
                    color: Colors.amberAccent,
                  ),
                  maxLines: 1,
                ),
              ),
              Container(child: Text('v0.1.0', textAlign: TextAlign.right, style: TextStyle(fontSize: 10.0),),),
              Container(
                width: MediaQuery.of(context).size.width * 0.75,
                child: Divider(
                  height: 25.0,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new MenuCard(
                    icon: Icons.calendar_today,
                    sizeIcon: 50.0,
                    title: 'Evènements',
                    cardColor: Color(0xFF8947a6),
                    route: '/eventslist',
                  ),
                  new MenuCard(
                    icon: Icons.speaker_group,
                    sizeIcon: 50.0,
                    title: 'Intervenants',
                    cardColor: Color(0xFF14927d),
                    route: '/speakerslist',
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  new MenuCard(
                    icon: Icons.people_outline,
                    sizeIcon: 50.0,
                    title: 'Participants',
                    cardColor: Color(0xFF61BEBF),
                    route: '/participantslist',
                  ),
                  new MenuCard(
                    icon: Icons.settings_applications,
                    sizeIcon: 50.0,
                    title: 'Paramètes',
                    cardColor: Colors.amberAccent,
                    route: '/settings',
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
