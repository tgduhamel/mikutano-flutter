import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SettingPage extends StatefulWidget {
  SettingPage({Key key}) : super(key: key);

  @override
  _SettingPageState createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  bool _brightValue = false;
  bool _session = false;

  @override
  void initState() {
    DynamicTheme.of(context).loadBrightness().then((value){
      setState(() {
        _brightValue = value;
      });
    });

    final storage = new FlutterSecureStorage();
    storage.read(key: 'apiToken').then(
          (value) {
        if (value != null) {
          setState(() {
            _session = true;
          });
        }
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Paramètres'),
        centerTitle: true,
        actions: <Widget>[],
      ),
      body: Padding(
        padding: EdgeInsets.all(10.0),
        child: Padding(
          padding: EdgeInsets.only(
            left: 10.0,
            right: 10.0,
            top: 5.0,
          ),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  top: 5.0,
                  bottom: 10.0,
                  left: 5.0,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('Thème mode nuit', style: TextStyle(fontSize: 18.0),),
                    Switch(
                      onChanged: (bool value) {
                        DynamicTheme.of(context).setBrightness(
                            value == true ? Brightness.dark : Brightness.light);
                        setState(() {
                          _brightValue = value;
                        });
                      },
                      value: _brightValue,
                    ),
                  ],
                ),
              ),
              /*
              Card(
                child: Container(
                  child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
                    leading: Icon(
                      Icons.person,
                    ),
                    title: Text(
                      "Informations personnelles",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      "Nom, téléphone, email, etc..",
                      style: TextStyle(
                        fontSize: 12.0,
                        wordSpacing: 0.0,
                        letterSpacing: 0.0,
                      ),
                    ),
                    trailing: Icon(
                      Icons.keyboard_arrow_right,
                      size: 30.0,
                    ),
                    onTap: () {},
                  ),
                ),
              ),
              Card(
                child: Container(
                  child: ListTile(
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 20.0, vertical: 1.0),
                    leading: Icon(
                      Icons.lock_open,
                    ),
                    title: Text(
                      "Mot de passe",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    subtitle: Text(
                      "Changer mon mot de passe",
                      style: TextStyle(
                        fontSize: 12.0,
                        wordSpacing: 0.0,
                        letterSpacing: 0.0,
                      ),
                    ),
                    trailing: Icon(
                      Icons.keyboard_arrow_right,
                      size: 30.0,
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, "/settings/password");
                    },
                  ),
                ),
              ),
              */
              Padding(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child: _session ? GestureDetector(
                  child: Text(
                    'Déconnexion',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFFB00020),
                      fontWeight: FontWeight.bold,
                      letterSpacing: 2.0,
                    ),
                  ),
                  onTap: () async {
                    showDialog(
                      context: context,
                      builder: (BuildContext ctx) {
                        return AlertDialog(
                          title: Text('Déconnexion'),
                          content: Text(
                              'Voulez-vous vraiment quitter votre session ?'),
                          actions: <Widget>[
                            FlatButton(
                              onPressed: () {
                                Navigator.of(ctx).pop();
                              },
                              child: Text('Annuler'),
                            ),
                            FlatButton(
                              onPressed: () async {
                                final storage = new FlutterSecureStorage();
                                await storage.deleteAll();
                                Navigator.popUntil(
                                    context, ModalRoute.withName('/'));
                                Navigator.pushReplacementNamed(context, '/');
                              },
                              child: Text('Quitter'),
                            ),
                          ],
                        );
                      },
                    );
                  },
                ) : null,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
