import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mikutano/event/participantdialog.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/models/Speaker.dart';
import 'package:mikutano/simple_participantcard.dart';

class PasswordForm extends StatefulWidget {
  @override
  _PasswordFormState createState() => _PasswordFormState();
}

class _PasswordFormState extends State<PasswordForm> {
  static GlobalKey<FormState> _changePwdFormKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Event'),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Form(
                  key: _changePwdFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Mot de passe actuel',
                          labelText: 'Mot de passe actuel',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                          return null;
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 25.0,
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Nouveau mot de passe',
                          labelText: 'Nouveau mot de passe',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                          return null;
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: 25.0,
                        ),
                      ),
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Confirmer le nouveau mot de passe',
                          labelText: 'Confirmer le nouveau mot de passe',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Stack(
        children: <Widget>[
          Positioned(
            bottom: 80.0,
            right: 0.0,
            child: FloatingActionButton(
              heroTag: 'save',
              onPressed: () {
                // Validate will return true if the form is valid, or false if
                // the form is invalid.
                if (_changePwdFormKey.currentState.validate()) {
                  // Process data.
                }
              },
              child: Icon(Icons.save),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            right: 0.0,
            child: FloatingActionButton(
              heroTag: 'close',
              onPressed: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.close),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
