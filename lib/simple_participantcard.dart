import 'package:flutter/material.dart';

class SimpleParticipantCard extends StatelessWidget {
  final String name;
  final String email;
  final String phone;

  const SimpleParticipantCard(
      {Key key,
      @required this.name,
      @required this.email,
      @required this.phone})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 180.0,
      height: 70.0,
      child: Card(
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(
                this.name,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 18.0),
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 2.0,
                  ),
                  Text(
                    this.email,
                    style: TextStyle(fontSize: 12.0),
                  ),
                  SizedBox(
                    height: 2.0,
                  ),
                  Text(
                    this.phone,
                    style: TextStyle(fontSize: 12.0),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
