import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:mikutano/event/eventform.dart';
import 'package:mikutano/event/eventlist.dart';
import 'package:mikutano/participant/participantform.dart';
import 'package:mikutano/participant/participantlist.dart';
import 'package:mikutano/settings/password.dart';
import 'package:mikutano/settings/settings.dart';
import 'package:mikutano/signin/SignIn.dart';
import 'package:mikutano/speaker/speakerform.dart';
import 'package:mikutano/speaker/speakerlist.dart';
import 'homepage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return DynamicTheme(
      defaultBrightness: Brightness.light,
      data: (brightness) {
        return brightness == Brightness.light ? getLightThemeData() : getDarkThemeData();
      },
      themedWidgetBuilder: (context, theme) {
        return MaterialApp(
          title: 'Simple Event App',
          initialRoute: '/',
          routes: {
            '/': (context) => HomePage(),
            '/signin': (context) => SignIn(),
            '/eventslist': (context) => EventList(),
            '/events/form': (context) => EventForm(),
            '/speakerslist': (context) => SpeakerList(),
            '/speakers/form': (context) => SpeakerForm(),
            '/participantslist': (context) => ParticipantList(),
            '/participants/form': (context) => ParticipantForm(),
            '/settings': (context) => SettingPage(),
            '/settings/password': (context) => PasswordForm(),
          },
          theme: theme,
        );
      },
    );
  }

  ThemeData getDarkThemeData() {
    return ThemeData(
          fontFamily: 'UbuntuMono',

          primaryColor: Color(0xFF339933),
          scaffoldBackgroundColor: Color(0xFF212121),
          primaryColorLight: Color(0xFF484848),
          primaryColorDark: Color(0xFF000000),
          accentColor: Color(0xFF339933),
          accentTextTheme: TextTheme(),
          backgroundColor: Color(0xFF212121),

          cardTheme: CardTheme(
            color: Color(0xFF000000),
          ),

          appBarTheme: AppBarTheme(
            color: Color(0xFF000000),
            iconTheme: IconThemeData(
              color: Color(0xFF338a3e),
            ),
            textTheme: TextTheme(
              title: TextStyle(color: Color(0xFF81c784), fontSize: 20.0),
            ),
          ),

          textTheme: TextTheme(
            headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            subhead: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Color(0xFF338a3e)),
            title: TextStyle(fontSize: 36.0, color: Color(0xFF338a3e)),
            body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind', color: Color(0xFF338a3e)),
            caption: TextStyle(color: Color(0xFF66bb6a)),
          ),

          iconTheme: IconThemeData(
            color: Color(0xFF338a3e),
          ),

          inputDecorationTheme: InputDecorationTheme(
            labelStyle: TextStyle(color: Color(0xFF338a3e)),
            focusedBorder: UnderlineInputBorder(borderSide: BorderSide( color: Color(0xFF338a3e),)),
          ),

          dividerColor: Color(0xFF338a3e),
          hintColor: Color(0xFF338a3e),
          textSelectionColor: Colors.white,
          cursorColor: Color(0xFF338a3e),
          textSelectionHandleColor: Color(0xFF338a3e),

          dialogTheme: DialogTheme(
              backgroundColor: Color(0xFF000000),
              elevation: 20.0,
              titleTextStyle: TextStyle(fontSize: 26.0, color: Color(0xFF338a3e)),
              contentTextStyle: TextStyle(fontSize: 16.0, color: Color(0xFF66bb6a))
          ),

          buttonTheme: ButtonThemeData(
              buttonColor: Colors.pink,
              textTheme: ButtonTextTheme.accent
          ),

          //splashColor: Color(0xFF263238),
          floatingActionButtonTheme: FloatingActionButtonThemeData(
            backgroundColor: Color(0xFF338a3e),
            foregroundColor: Color(0xFF000000),
          ),
        );
  }

  ThemeData getLightThemeData() {
    return ThemeData(
      fontFamily: 'UbuntuMono',

      primaryColor: Color(0xFF339933),
      primaryColorLight: Color(0xFF66bb6a),
      primaryColorDark: Color(0xFF339933),
      accentColor: Color(0xFF212121),
      accentTextTheme: TextTheme(),
      backgroundColor: Colors.white,

      textTheme: TextTheme(
        headline: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
        subhead: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold,),
        title: TextStyle(fontSize: 36.0,),
        body1: TextStyle(fontSize: 14.0, fontFamily: 'Hind',),
      ),

      iconTheme: IconThemeData(
        color: Color(0xFF212121),
      ),

      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(color: Color(0xFF212121)),
        focusedBorder: UnderlineInputBorder(borderSide: BorderSide( color: Color(0xFF212121),)),
      ),

      hintColor: Color(0xFF212121),
      textSelectionColor: Color(0xFF484848),
      cursorColor: Color(0xFF212121),
      textSelectionHandleColor: Color(0xFF212121),

      dialogTheme: DialogTheme(
          elevation: 20.0,
          titleTextStyle: TextStyle(fontSize: 26.0, color: Color(0xFF212121)),
          contentTextStyle: TextStyle(fontSize: 16.0, color: Color(0xFF484848))
      ),

      floatingActionButtonTheme: FloatingActionButtonThemeData(
        backgroundColor: Colors.white,
        foregroundColor: Color(0xFF000000),
      ),
    );
  }
}