import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mikutano/event/participantdialog.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/helper/DateManagement.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/simple_participantcard.dart';
import 'package:pk_skeleton/pk_skeleton.dart';

class EventForm extends StatefulWidget {
  final int idEvent;

  EventForm({this.idEvent});

  @override
  _EventFormState createState() => _EventFormState();
}

class _EventFormState extends State<EventForm> {
  static GlobalKey<FormState> _eventFormKey = GlobalKey<FormState>();
  static GlobalKey<ScaffoldState> _eventFormScaffoldKey =
      GlobalKey<ScaffoldState>();

  int idEventToEdit = 0;
  bool _loadingData = false;
  bool _saving = false;
  bool _hasDataChanged = false;

  var titleCtr = TextEditingController();
  var addressCtr = TextEditingController();
  var priceCtr = TextEditingController();
  var noteCtr = TextEditingController();

  DateTime startDate;
  var startDateCtr = TextEditingController();
  TimeOfDay startHour;
  var startHourCtr = TextEditingController();

  DateTime endDate;
  var endDateCtr = TextEditingController();
  TimeOfDay endHour;
  var endHourCtr = TextEditingController();

  List<Participant> speakers = [];
  List<Participant> participants = [];

  OverlayState _progressOverlay;
  OverlayEntry _progressEntry;

  @override
  void initState() {
    _progressOverlay = Overlay.of(context);
    if (widget.idEvent != null) {
      //edit mode
      idEventToEdit = widget.idEvent;
      //@todo fetch event data
      _loadEvent(idEventToEdit);
    }
    super.initState();
  }

  void _loadEvent(id) {
    setState(() {
      _loadingData = true;
    });
    //we fetch data directly link to the event
    fetchDatas('http://3.125.208.58:3081/api/conferences/$id').then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 200) {
        final parsed = jsonDecode(value.body);
        print(parsed);
        setState(() {
          titleCtr.text = parsed['title'];
          addressCtr.text = parsed['address'];
          priceCtr.text = parsed['price'].toString();
          startDateCtr.text = fromYearToDay(parsed['dateStart']);
          startHourCtr.text = parsed['hourStart'];
          endDateCtr.text = fromYearToDay(parsed['dateEnd']);
          endHourCtr.text = parsed['hourEnd'];
          noteCtr.text = parsed['note'];
          participants = parsed['participants']
              .map<Participant>((json) => Participant.fromJson(json))
              .toList();
          speakers = parsed['speakers']
              .map<Participant>((json) => Participant.fromJson(json))
              .toList();
        });
      } else {
        print(value.body);
        _eventFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données.',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).whenComplete(() {
      setState(() {
        _loadingData = false;
      });
    });
  }

  OverlayEntry _createProgressEntry() {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;

    _progressEntry = OverlayEntry(
        builder: (context) => Positioned(
              top: 25.0,
              width: size.width,
              child: Material(
                elevation: 4.0,
                child: LinearProgressIndicator(
                  value: null,
                ),
              ),
            ));

    return _progressEntry;
  }

  void _insertProgressEntry() {
    _progressOverlay.insert(_createProgressEntry());
  }

  void _removeProgressEntry() {
    _progressEntry.remove();
  }

  @override
  void dispose() {
    titleCtr.dispose();
    addressCtr.dispose();
    priceCtr.dispose();
    noteCtr.dispose();
    startDateCtr.dispose();
    startHourCtr.dispose();
    endDateCtr.dispose();
    endHourCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _eventFormScaffoldKey,
      appBar: AppBar(
        title: idEventToEdit == 0 ? Text('Event') : Text("${titleCtr.text}"),
        centerTitle: true,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, _hasDataChanged);
          },
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
              top: 10.0,
              bottom: 60.0,
              left: 10.0,
              right: 10.0,
            ),
            child: _loadingData
                ? PKCardSkeleton(
                    isBottomLinesActive: true,
                  )
                : Column(
                    children: <Widget>[
                      Form(
                        key: _eventFormKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            TextFormField(
                              controller: titleCtr,
                              decoration: const InputDecoration(
                                icon: Icon(Icons.calendar_today),
                                hintText: 'Titre complet de l\'évènemment',
                                labelText: 'Titre *',
                              ),
                              maxLines: 2,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                                return null;
                              },
                            ),
                            TextFormField(
                              controller: addressCtr,
                              decoration: const InputDecoration(
                                icon: Icon(Icons.location_on),
                                hintText: 'Adresse complète de l\'évènemment',
                                labelText: 'Adresse *',
                              ),
                              maxLines: 2,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                                return null;
                              },
                            ),
                            TextFormField(
                              controller: priceCtr,
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                icon: Icon(Icons.euro_symbol),
                                hintText: 'Ex: 15.50',
                                labelText: 'Entrée (PAF) *',
                              ),
                              validator: (value) {
                                if (value.isEmpty) {
                                  return '';
                                }
                                return null;
                              },
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                top: 10.0,
                                bottom: 10.0,
                              ),
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Icon(
                                        Icons.access_time,
                                      ),
                                    ),
                                  ),
                                  Expanded(child: Text(' Du ')),
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        TextFormField(
                                          decoration: const InputDecoration(
                                            labelText: 'Date',
                                            hintText: '12/11/2019',
                                          ),
                                          readOnly: true,
                                          controller: startDateCtr,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return '';
                                            }
                                            return null;
                                          },
                                          onTap: () async {
                                            startDate = await showDatePicker(
                                              context: context,
                                              initialDate:
                                                  startDate ?? DateTime.now(),
                                              firstDate: DateTime(
                                                DateTime.now().year,
                                                DateTime.now().month,
                                                DateTime.now().day,
                                              ),
                                              lastDate: DateTime.now()
                                                  .add(Duration(days: 3600)),
                                            );
                                            setState(() {
                                              if (startDate != null) {
                                                startDateCtr.text =
                                                    stringToHumanDate(
                                                        startDate.year,
                                                        startDate.month,
                                                        startDate.day);
                                              }
                                            });
                                          },
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text('à'),
                                        ),
                                        TextFormField(
                                          decoration: const InputDecoration(
                                            labelText: 'Heure',
                                            hintText: '15:30',
                                          ),
                                          readOnly: true,
                                          controller: startHourCtr,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return '';
                                            }
                                            return null;
                                          },
                                          onTap: () async {
                                            startHour = await showTimePicker(
                                              context: context,
                                              initialTime: TimeOfDay.now(),
                                            );
                                            setState(() {
                                              if (startHour != null) {
                                                startHourCtr.text =
                                                    stringToHumanTime(
                                                        startHour.hour,
                                                        startHour.minute);
                                              }
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                  Expanded(child: Text(' Au ')),
                                  Expanded(
                                    flex: 3,
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        TextFormField(
                                          decoration: const InputDecoration(
                                            labelText: 'Date',
                                            hintText: '12/11/2019',
                                          ),
                                          readOnly: true,
                                          controller: endDateCtr,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return '';
                                            }
                                            return null;
                                          },
                                          onTap: () async {
                                            endDate = await showDatePicker(
                                              context: context,
                                              initialDate:
                                                  endDate ?? DateTime.now(),
                                              firstDate: DateTime(
                                                DateTime.now().year,
                                                DateTime.now().month,
                                                DateTime.now().day,
                                              ),
                                              lastDate: DateTime.now()
                                                  .add(Duration(days: 3600)),
                                            );
                                            setState(() {
                                              if (endDate != null) {
                                                endDateCtr.text =
                                                    stringToHumanDate(
                                                        endDate.year,
                                                        endDate.month,
                                                        endDate.day);
                                              }
                                            });
                                          },
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(top: 10.0),
                                          child: Text('à'),
                                        ),
                                        TextFormField(
                                          decoration: const InputDecoration(
                                            labelText: 'Heure',
                                            hintText: '20:00',
                                          ),
                                          readOnly: true,
                                          controller: endHourCtr,
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return '';
                                            }
                                            return null;
                                          },
                                          onTap: () async {
                                            endHour = await showTimePicker(
                                              context: context,
                                              initialTime: TimeOfDay.now(),
                                            );
                                            setState(() {
                                              if (endHour != null) {
                                                endHourCtr.text =
                                                    stringToHumanTime(
                                                        endHour.hour,
                                                        endHour.minute);
                                              }
                                            });
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            TextFormField(
                              controller: noteCtr,
                              keyboardType: TextInputType.multiline,
                              decoration: const InputDecoration(
                                icon: Icon(Icons.message),
                                hintText: 'Note ou observation',
                                labelText: 'Note',
                              ),
                              validator: (value) {
                                return null;
                              },
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                top: 25.0,
                              ),
                            ),
                            ExpansionTile(
                              title: Text(
                                  'Liste des intervenants (${speakers.length})'),
                              leading: Icon(
                                Icons.speaker_group,
                              ),
                              onExpansionChanged: (bool extended) {
                                if (extended == true) {}
                              },
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.add,
                                        size: 30.0,
                                      ),
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (ctx) {
                                            return ParticipantDialog(
                                              resource: 'speakers',
                                              onSelectionNameChanged:
                                                  (speaker) {
                                                if (!this
                                                    .speakers
                                                    .contains(speaker)) {
                                                  setState(() {
                                                    this.speakers.add(speaker);
                                                  });
                                                } else {
                                                  _eventFormScaffoldKey
                                                      .currentState
                                                      .showSnackBar(
                                                    SnackBar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      content: Text(
                                                        'Déjà existant !',
                                                        style: TextStyle(
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                }
                                              },
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Wrap(
                                    children: speakers
                                        .map((Participant speaker) {
                                          return GestureDetector(
                                            child: SimpleParticipantCard(
                                              name: speaker.name,
                                              email: speaker.mail,
                                              phone: speaker.phone,
                                            ),
                                            onLongPress: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext ctx) {
                                                  return AlertDialog(
                                                    title: Text('Retirer ?'),
                                                    content: Text(
                                                        'Retirer de la liste'),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text('Non'),
                                                      ),
                                                      FlatButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            speakers.remove(
                                                                speaker);
                                                          });
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text('Oui'),
                                                      ),
                                                    ],
                                                  );
                                                },
                                                barrierDismissible: false,
                                              );
                                            },
                                          );
                                        })
                                        .toList()
                                        .cast<Widget>(),
                                  ),
                                ),
                              ],
                            ),
                            ExpansionTile(
                              title: Text(
                                  'Liste des participants (${participants.length})'),
                              leading: Icon(
                                Icons.people_outline,
                              ),
                              onExpansionChanged: (bool extended) {
                                if (extended == true) {}
                              },
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      icon: Icon(
                                        Icons.add,
                                        size: 30.0,
                                      ),
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (ctx) {
                                            return ParticipantDialog(
                                              resource: 'participants',
                                              onSelectionNameChanged:
                                                  (participant) {
                                                if (!this
                                                    .participants
                                                    .contains(participant)) {
                                                  setState(() {
                                                    this
                                                        .participants
                                                        .add(participant);
                                                  });
                                                } else {
                                                  _eventFormScaffoldKey
                                                      .currentState
                                                      .showSnackBar(
                                                    SnackBar(
                                                      backgroundColor:
                                                          Colors.white,
                                                      content: Text(
                                                        'Déjà existant !',
                                                        style: TextStyle(
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ),
                                                  );
                                                }
                                              },
                                            );
                                          },
                                        );
                                      },
                                    ),
                                  ],
                                ),
                                Container(
                                  child: Wrap(
                                    children: participants
                                        .map((Participant participant) {
                                          return GestureDetector(
                                            child: SimpleParticipantCard(
                                              name: participant.name,
                                              email: participant.mail,
                                              phone: participant.phone,
                                            ),
                                            onLongPress: () {
                                              showDialog(
                                                context: context,
                                                builder: (BuildContext ctx) {
                                                  return AlertDialog(
                                                    title: Text('Retirer ?'),
                                                    content: Text(
                                                        'Retirer de la liste ?'),
                                                    actions: <Widget>[
                                                      FlatButton(
                                                        onPressed: () {
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text('Non'),
                                                      ),
                                                      FlatButton(
                                                        onPressed: () {
                                                          setState(() {
                                                            participants.remove(
                                                                participant);
                                                          });
                                                          Navigator.pop(
                                                              context);
                                                        },
                                                        child: Text('Oui'),
                                                      ),
                                                    ],
                                                  );
                                                },
                                                barrierDismissible: false,
                                              );
                                            },
                                          );
                                        })
                                        .toList()
                                        .cast<Widget>(),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
          ),
        ),
      ),
      floatingActionButton: Stack(
        children: _loadingData
            ? <Widget>[]
            : <Widget>[
                Positioned(
                  bottom: 0.0,
                  right: 70.0,
                  child: idEventToEdit == 0
                      ? Container(width: 0.0, height: 0.0)
                      : FloatingActionButton(
                          heroTag: 'delete',
                          backgroundColor: Colors.white,
                          onPressed: _saving
                              ? null
                              : () {
                                  _eventFormScaffoldKey.currentState
                                      .showSnackBar(
                                    SnackBar(
                                      content: Text(
                                        'Action non définie pour le moment.',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                      backgroundColor: Color(0xFF000000),
                                    ),
                                  );
                                },
                          child: Icon(
                            Icons.delete,
                            color: Color(0xFFB00020),
                          ),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                ),
                Positioned(
                  bottom: 70.0,
                  right: 0.0,
                  child: FloatingActionButton(
                    heroTag: 'save',
                    onPressed: _saving
                        ? null
                        : () {
                            if (_eventFormKey.currentState.validate()) {
                              setState(() {
                                _saving = true;
                                _insertProgressEntry();
                              });

                              var body = {
                                "title": "${titleCtr.text}",
                                "address": "${addressCtr.text}",
                                "dateStart":
                                    "${fromDayToYear(startDateCtr.text)}",
                                "hourStart": "${startHourCtr.text}",
                                "dateEnd": "${fromDayToYear(endDateCtr.text)}",
                                "hourEnd": "${endHourCtr.text}",
                                "price": priceCtr.text,
                                "participants":
                                    participants.map((Participant participant) {
                                  return participant.uri;
                                }).toList(),
                                "speakers":
                                    speakers.map((Participant participant) {
                                  return participant.uri;
                                }).toList(),
                              };

                              print(jsonEncode(body));

                              idEventToEdit == 0
                                  ? createEvent(body, context)
                                  : updateEvent(body, context, idEventToEdit);
                            }
                          },
                    child: Icon(Icons.save),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0.0,
                  right: 0.0,
                  child: FloatingActionButton(
                    heroTag: 'close',
                    onPressed: () {
                      Navigator.pop(context, _hasDataChanged);
                    },
                    child: Icon(Icons.arrow_back),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                ),
              ],
      ),
    );
  }

  void createEvent(Map<String, Object> body, BuildContext context) {
    postDatas('http://3.125.208.58:3081/api/conferences', jsonEncode(body))
        .then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 201 || value.statusCode == 200) {
        _eventFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Données enregistrées.',
            ),
            backgroundColor: Color(0xFF339933),
          ),
        );
        final parsed = (jsonDecode(value.body));
        setState(() {
          idEventToEdit = parsed['id'];
          _hasDataChanged = true;
        });
      } else {
        print(value.body);
        _eventFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible d\'enregistrer les données : ${(jsonDecode(value.body)['hydra:description']).toString()}',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _eventFormScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    }).whenComplete(() {
      setState(() {
        _saving = false;
        _removeProgressEntry();
      });
    });
  }

  void updateEvent(
      Map<String, Object> body, BuildContext context, int idEvent) {
    putDatas('http://3.125.208.58:3081/api/conferences/$idEvent',
            jsonEncode(body))
        .then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 201 || value.statusCode == 200) {
        _eventFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Données enregistrées.',
            ),
            backgroundColor: Color(0xFF339933),
          ),
        );
        setState(() {
          _hasDataChanged = true;
        });
      } else {
        print(value.body);
        _eventFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible d\'enregistrer les données : ${(jsonDecode(value.body)['hydra:description']).toString()}',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _eventFormScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    }).whenComplete(() {
      setState(() {
        _saving = false;
        _removeProgressEntry();
      });
    });
  }
}
