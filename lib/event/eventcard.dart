import 'package:flutter/material.dart';
import 'package:mikutano/models/MEvent.dart';

import 'eventform.dart';

class EventCard extends StatelessWidget {
  final int id;
  final String title;
  final String address;
  final String startDate;
  final String startHour;
  final String endDate;
  final String endHour;
  final int nSpeakers;
  final int nParticipants;

  const EventCard(
      {Key key,
      @required this.id,
      @required this.title,
      @required this.address,
      @required this.startDate,
      @required this.startHour,
      @required this.endDate,
      @required this.endHour,
      @required this.nSpeakers,
      @required this.nParticipants})
      : super(key: key);

  factory EventCard.fromMEvent(MEvent event) {
    return EventCard(
      id: event.key,
      title: event.title,
      address: event.address,
      startDate: event.startDate,
      startHour: event.startHour,
      endDate: event.endDate,
      endHour: event.endHour,
      nSpeakers: event.speakers.length,
      nParticipants: event.participants.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            this.title.toUpperCase(),
            style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.normal),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Divider(
          height: 15.0,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 5.0, bottom: 15.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Icon(
                          Icons.location_on,
                          size: 13.0,
                        ),
                      ),
                      Text(this.address,
                          style: TextStyle(
                            /*color: Color(0xFF616161),*/
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Icon(
                          Icons.access_time,
                          size: 13.0,
                        ),
                      ),
                      Row(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(this.startDate,
                                  style: TextStyle(
                                    /*color: Color(0xFF616161),*/
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w700,
                                  )),
                              Text(this.startHour,
                                  style: TextStyle(
                                    /*color: Color(0xFF616161),*/
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w700,
                                  )),
                            ],
                          ),
                          Text(' - '),
                          Column(
                            children: <Widget>[
                              Text(this.endDate,
                                  style: TextStyle(
                                    /*color: Color(0xFF616161),*/
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w700,
                                  )),
                              Text(this.endHour,
                                  style: TextStyle(
                                    /*color: Color(0xFF616161),*/
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.w700,
                                  )),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Icon(
                          Icons.speaker_group,
                          size: 13.0,
                        ),
                      ),
                      Text(
                          this.nSpeakers.toString() +
                              ((this.nSpeakers > 1)
                                  ? " intervenants"
                                  : " intervenant"),
                          style: TextStyle(
                            /*color: Color(0xFF616161),*/
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Icon(
                          Icons.people,
                          size: 13.0,
                        ),
                      ),
                      Text(
                          this.nParticipants.toString() +
                              ((this.nParticipants > 1)
                                  ? " participants"
                                  : " participant"),
                          style: TextStyle(
                            /*color: Color(0xFF616161),*/
                            fontSize: 10.0,
                            fontWeight: FontWeight.w700,
                          )),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
