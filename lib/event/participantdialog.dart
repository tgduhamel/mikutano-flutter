import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/models/Participant.dart';

class ParticipantDialog extends StatefulWidget {
  final ValueChanged<Participant> onSelectionNameChanged;
  String resource = 'speakers';

  ParticipantDialog({this.onSelectionNameChanged, this.resource});

  @override
  _ParticipantDialogState createState() => _ParticipantDialogState();
}

class _ParticipantDialogState extends State<ParticipantDialog> {
  static GlobalKey<FormState> _participantdialogformKey =
      GlobalKey<FormState>();
  static GlobalKey<_ParticipantDialogState> _dialogformKey =
      GlobalKey<_ParticipantDialogState>();
  Participant participantValue;
  bool _loading = true;

  List<Participant> _people = [];

  @override
  void initState() {
    fetchDatas('http://3.125.208.58:3081/api/${widget.resource}').then((value) {
      if (value.statusCode == 200) {
        setState(() {
          _loading = false;
        });
        final parsed = (jsonDecode(value.body)['hydra:member'])
            .cast<Map<String, dynamic>>();
        _people = parsed
            .map<Participant>((json) => Participant.fromJson(json))
            .toList();
      } else {
        print(value.body);
        _dialogformKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données. ${value.body}',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Dialog(
        key: _dialogformKey,
        child: Form(
          key: _participantdialogformKey,
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: _loading
                ? Container(
                    height: 5.0,
                    child: LinearProgressIndicator(
                      value: null,
                    ),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 70.0,
                        child: DropdownButton(
                          hint: Text('Sélectionner un nom'),
                          isExpanded: true,
                          icon: Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          value: participantValue,
                          onChanged: (Participant newValue) {
                            setState(() {
                              if (newValue != null) {
                                participantValue = newValue;
                              }
                            });
                          },
                          items: _people.map((Participant participant) {
                            return DropdownMenuItem<Participant>(
                              value: participant,
                              child: Text(
                                  '${participant.name} (${participant.phone})'),
                            );
                          }).toList(),
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          FlatButton(
                            onPressed: () {
                              Navigator.pop(context);
                            },
                            child: Text('Annuler'),
                          ),
                          FlatButton(
                            onPressed: () {
                              if (participantValue != null) {
                                widget.onSelectionNameChanged(participantValue);
                                Navigator.pop(context);
                              }
                            },
                            child: Text('Ok'),
                          ),
                        ],
                      ),
                    ],
                  ),
          ),
        ),
      ),
    );
  }

  void showSnackBar(SnackBar snackBar) {}
}
