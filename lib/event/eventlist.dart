import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/models/MEvent.dart';
import 'package:pk_skeleton/pk_skeleton.dart';

import 'eventcard.dart';
import 'eventform.dart';

class EventList extends StatefulWidget {
  EventList({Key key}) : super(key: key);

  @override
  _EventListState createState() => _EventListState();
}

class _EventListState extends State<EventList> {
  static GlobalKey<ScaffoldState> _eventsListScaffoldKey =
      GlobalKey<ScaffoldState>();
  List<MEvent> _eventsList = [];

  bool _loading = true;
  bool _hasDataChanged = false;

  @override
  void initState() {
    getEventsList();
    super.initState();
  }

  getEventsList() {
    return fetchDatas('http://3.125.208.58:3081/api/conferences').then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 200) {
        final parsed = (jsonDecode(value.body)['hydra:member'])
            .cast<Map<String, dynamic>>();
        setState(() {
          _loading = false;
          _eventsList =
              parsed.map<MEvent>((json) => MEvent.fromJson(json)).toList();
        });
      } else {
        print(value.body);
        _eventsListScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données.',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _eventsListScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _eventsListScaffoldKey,
      appBar: AppBar(
        title: Text('Liste évènements (${_eventsList.length})'),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(5.0),
        child: _loading
            ? PKCardSkeleton(
                isBottomLinesActive: true,
              )
            : _eventsList.length == 0
                ? Center(
                    child: Text(
                      'Aucune donnée disponible.',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                : RefreshIndicator(
                    child: ListView(
                      physics: AlwaysScrollableScrollPhysics(),
                      children: _eventsList
                          .map((MEvent event) {
                            return Card(
                              child: FlatButton(
                                child: new EventCard.fromMEvent(event),
                                onPressed: () async {
                                  _hasDataChanged = await Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => EventForm(
                                        idEvent: event.key,
                                      ),
                                    ),
                                  );

                                  if (_hasDataChanged != null && _hasDataChanged) {
                                    getEventsList();
                                  }
                                },
                              ),
                            );
                          })
                          .toList()
                          .cast<Widget>(),
                    ),
                    onRefresh: () {
                      return getEventsList();
                    },
                  ),
      ),
      floatingActionButton: _loading
          ? null
          : FloatingActionButton(
              onPressed: () async {
                _hasDataChanged = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EventForm(),
                  ),
                );

                if (_hasDataChanged != null && _hasDataChanged) {
                  getEventsList();
                }
              },
              child: Icon(Icons.add),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0)),
            ),
    );
  }
}
