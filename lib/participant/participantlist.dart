import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/participant/participantform.dart';
import 'package:pk_skeleton/pk_skeleton.dart';
import 'participantcard.dart';

class ParticipantList extends StatefulWidget {
  ParticipantList({Key key}) : super(key: key);

  @override
  _ParticipantListState createState() => _ParticipantListState();
}

class _ParticipantListState extends State<ParticipantList> {
  static GlobalKey<ScaffoldState> _participantListScaffoldKey =
      GlobalKey<ScaffoldState>();
  List<Participant> _speakersList = [];

  bool _loading = true;
  bool _hasDataChanged = false;

  @override
  void initState() {
    getParticipantsList();
    super.initState();
  }

  getParticipantsList() {
    return fetchDatas('http://3.125.208.58:3081/api/participants')
        .then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 200) {
        setState(() {
          _loading = false;
        });
        final parsed = (jsonDecode(value.body)['hydra:member'])
            .cast<Map<String, dynamic>>();
        _speakersList = parsed
            .map<Participant>((json) => Participant.fromJson(json))
            .toList();
      } else {
        print(value.body);
        _participantListScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données.',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _participantListScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _participantListScaffoldKey,
      appBar: AppBar(
        title: Text('Liste participants (${_speakersList.length})'),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(5.0),
        child: _loading
            ? PKCardSkeleton(
                isBottomLinesActive: true,
              )
            : _speakersList.length == 0
                ? Center(
                    child: Text(
                      'Aucune donnée disponible.',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )
                : RefreshIndicator(
                    child: ListView(
                      physics: AlwaysScrollableScrollPhysics(),
                      children: _speakersList
                          .map((Participant participant) {
                            return Card(
                              child: FlatButton(
                                  child:
                                      new ParticipantCard.fromJson(participant),
                                  onPressed: () async {
                                    _hasDataChanged = await Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => ParticipantForm(
                                          idParticipant: participant.id,
                                        ),
                                      ),
                                    );

                                    if (_hasDataChanged != null &&
                                        _hasDataChanged) {
                                      getParticipantsList();
                                    }
                                  }),
                            );
                          })
                          .toList()
                          .cast<Widget>(),
                    ),
                    onRefresh: () {
                      return getParticipantsList();
                    },
                  ),
      ),
      floatingActionButton: _loading
          ? null
          : FloatingActionButton(
              onPressed: () async {
                _hasDataChanged = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ParticipantForm(),
                  ),
                );

                if (_hasDataChanged != null && _hasDataChanged) {
                  getParticipantsList();
                }
              },
              child: Icon(Icons.add),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
    );
  }
}
