import 'package:flutter/material.dart';
import 'package:mikutano/models/Participant.dart';
import 'package:mikutano/participant/participantform.dart';

class ParticipantCard extends StatelessWidget {
  final int id;
  final String name;
  final String email;
  final String phone;

  const ParticipantCard(
      {Key key,
      @required this.id,
      @required this.name,
      @required this.email,
      @required this.phone})
      : super(key: key);

  factory ParticipantCard.fromJson(Participant participant) {
    return ParticipantCard(
        id: participant.id,
        name: participant.name,
        email: participant.mail,
        phone: participant.phone);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Text(
                this.name,
                style: TextStyle(
                    fontSize: 25.0, fontWeight: FontWeight.normal),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, bottom: 5.0),
              child: Text(
                this.email,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, bottom: 5.0),
              child: Text(
                this.phone,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
