import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:mikutano/helper/ApiCall.dart';
import 'package:pk_skeleton/pk_skeleton.dart';

class ParticipantForm extends StatefulWidget {
  final int idParticipant;

  ParticipantForm({this.idParticipant});
  
  @override
  _ParticipantFormState createState() => _ParticipantFormState();
}

class _ParticipantFormState extends State<ParticipantForm> {
  static GlobalKey<FormState> _eventFormKey = GlobalKey<FormState>();
  static GlobalKey<ScaffoldState> _speakerFormScaffoldKey =
  GlobalKey<ScaffoldState>();

  int idParticipantToEdit = 0;
  bool _loadingData = false;
  bool _saving = false;
  bool _hasDataChanged = false;

  var nameCtr = TextEditingController();
  var mailCtr = TextEditingController();
  var phoneCtr = TextEditingController();

  OverlayState _progressOverlay;
  OverlayEntry _progressEntry;

  @override
  void initState() {
    _progressOverlay = Overlay.of(context);
    if (widget.idParticipant != null) {
      //edit mode
      idParticipantToEdit = widget.idParticipant;
      _loadParticipant(idParticipantToEdit);
    }
    super.initState();
  }

  void _loadParticipant(id) {
    setState(() {
      _loadingData = true;
    });
    //we fetch data directly link to the event
    fetchDatas('http://3.125.208.58:3081/api/participants/$id').then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 200) {
        final parsed = jsonDecode(value.body);
        print(parsed);
        setState(() {
          nameCtr.text = parsed['name'];
          mailCtr.text = parsed['email'];
          phoneCtr.text = parsed['phone'];
        });
      } else {
        print(value.body);
        _speakerFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible de recupérer les données.',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).whenComplete(() {
      setState(() {
        _loadingData = false;
      });
    });
  }

  OverlayEntry _createProgressEntry() {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;

    _progressEntry = OverlayEntry(
        builder: (context) => Positioned(
          top: 25.0,
          width: size.width,
          child: Material(
            elevation: 4.0,
            child: LinearProgressIndicator(value: null,),
          ),
        ));

    return _progressEntry;
  }

  void _insertProgressEntry() {
    _progressOverlay.insert(_createProgressEntry());
  }

  void _removeProgressEntry() {
    _progressEntry.remove();
  }

  @override
  void dispose() {
    nameCtr.dispose();
    mailCtr.dispose();
    phoneCtr.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _speakerFormScaffoldKey,
      appBar: AppBar(
        title: idParticipantToEdit == 0 ? Text('Participant') : Text("${nameCtr.text}"),
        centerTitle: true,
        leading: new IconButton(
          icon: new Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, _hasDataChanged);
          },
        ),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: _loadingData
                ? PKCardSkeleton(
              isBottomLinesActive: true,
            )
                : Column(
              children: <Widget>[
                Form(
                  key: _eventFormKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        controller: nameCtr,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.person),
                          hintText: 'Nom complet du participant',
                          labelText: 'Nom *',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return '';
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: mailCtr,
                        keyboardType: TextInputType.emailAddress,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.mail),
                          hintText: 'Ex: mon.mail@mkt.com',
                          labelText: 'Adresse mail',
                        ),
                      ),
                      TextFormField(
                        controller: phoneCtr,
                        keyboardType: TextInputType.phone,
                        decoration: const InputDecoration(
                          icon: Icon(Icons.phone),
                          hintText: 'Ex: 07XXXXXX00',
                          labelText: 'Numéro de téléphone',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: Stack(
        children: _loadingData
            ? <Widget>[]
            : <Widget>[
          Positioned(
            bottom: 0.0,
            right: 70.0,
            child: idParticipantToEdit == 0
                ? Container(width: 0.0, height: 0.0)
                : FloatingActionButton(
              heroTag: 'delete',
              backgroundColor: Colors.white,
              onPressed: _saving ? null : () {
                _speakerFormScaffoldKey.currentState.showSnackBar(
                  SnackBar(
                    content: Text(
                      'Action non définie pour le moment.',
                      style: TextStyle(color: Colors.white,),
                    ),
                    backgroundColor: Color(0xFF000000),
                  ),
                );
              },
              child: Icon(
                Icons.delete,
                color: Color(0xFFB00020),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
          Positioned(
            bottom: 80.0,
            right: 0.0,
            child: FloatingActionButton(
              heroTag: 'save',
              onPressed: _saving
                  ? null
                  : () {
                if (_eventFormKey.currentState.validate()) {
                  setState(() {
                    _saving = true;
                    _insertProgressEntry();
                  });

                  var body = {
                    "name": "${nameCtr.text}",
                    "email": "${mailCtr.text}",
                    "phone": phoneCtr.text,
                  };

                  print(jsonEncode(body));

                  idParticipantToEdit == 0
                      ? createEvent(body, context)
                      : updateEvent(body, context, idParticipantToEdit);
                }
              },
              child: Icon(Icons.save),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
          Positioned(
            bottom: 0.0,
            right: 0.0,
            child: FloatingActionButton(
              heroTag: 'close',
              onPressed: () {
                Navigator.pop(context, _hasDataChanged);
              },
              child: Icon(Icons.close),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void createEvent(Map<String, Object> body, BuildContext context) {
    postDatas('http://3.125.208.58:3081/api/participants', jsonEncode(body))
        .then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 201 || value.statusCode == 200) {
        _speakerFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Données enregistrées.',
            ),
            backgroundColor: Color(0xFF339933),
          ),
        );
        final parsed = (jsonDecode(value.body));
        setState(() {
          idParticipantToEdit = parsed['id'];
          _hasDataChanged = true;
        });
      } else {
        print(value.body);
        _speakerFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible d\'enregistrer les données : ${(jsonDecode(value.body)['hydra:description']).toString()}',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _speakerFormScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    }).whenComplete(() {
      setState(() {
        _saving = false;
        _removeProgressEntry();
      });
    });
  }

  void updateEvent(
      Map<String, Object> body, BuildContext context, int idEvent) {
    putDatas('http://3.125.208.58:3081/api/participants/$idEvent',
        jsonEncode(body))
        .then((value) {
      if (value.statusCode == 403) {
        Navigator.pushReplacementNamed(context, '/signin');
      } else if (value.statusCode == 201 || value.statusCode == 200) {
        _speakerFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Données enregistrées.',
            ),
            backgroundColor: Color(0xFF339933),
          ),
        );
        setState(() {
          _hasDataChanged = true;
        });
      } else {
        print(value.body);
        _speakerFormScaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text(
              'Impossible d\'enregistrer les données : ${(jsonDecode(value.body)['hydra:description']).toString()}',
            ),
            backgroundColor: Color(0xFFB00020),
          ),
        );
      }
    }).catchError((e) {
      print("Got an error : ${e}");
      _speakerFormScaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text(
            'Erreur de connection au serveur.',
          ),
          backgroundColor: Color(0xFFB00020),
        ),
      );
    }).whenComplete(() {
      setState(() {
        _saving = false;
        _removeProgressEntry();
      });
    });
  }
}
